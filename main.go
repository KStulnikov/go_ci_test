package main

import (
	"github.com/sirupsen/logrus"
	"os"
)

var (
	BuildDate     string
	BuildNumber   string
	ServerVersion string
	CommitHash    string
)

func main() {

	logger := logrus.New()
	logger.Out = os.Stdout
	logger.SetReportCaller(true)
	logger.Formatter = &logrus.JSONFormatter{TimestampFormat: "2006-01-02T15:04:05.999999999Z07:00", PrettyPrint: false}
	logger.Level = logrus.InfoLevel

	logFields := logrus.Fields{}
	logFields["build_date"] = BuildDate
	logFields["build_number"] = BuildNumber
	logFields["version"] = ServerVersion
	logFields["commit_hash"] = CommitHash

	logger.WithFields(logFields).Errorf("err")
}
