FROM debian:buster-slim

ARG HTTP_PROXY
ARG HTTPS_PROXY
ARG NO_PROXY
ARG http_proxy=$HTTP_PROXY
ARG https_proxy=$HTTPS_PROXY
ARG no_proxy=$NO_PROXY

ARG BUILD_DATE
ARG BUILD_CI_LINK
ARG BUILD_PROJECT_LINK
ARG BUILD_VERSION
ARG GIT_COMMIT

ARG DEBIAN_FRONTEND="noninteractive"

ENV WEB_LISTEN_ADDRESS=":8081"

RUN useradd -m -s /bin/sh appuser

COPY builded_app /bucketapi

RUN mkdir /files
RUN chmod -R 777 /files

USER appuser

EXPOSE 8030/tcp
EXPOSE 8031/tcp

ENTRYPOINT ["/bucketapi"]


ENV CI_BUILD_DATE=${BUILD_DATE}
ENV CI_BUILD_VERSION=${BUILD_VERSION}
ENV CI_BUILD_JOB_LINK=${BUILD_CI_LINK}

LABEL org.label-schema.build-date="${BUILD_DATE}" \
	org.label-schema.build-version="${BUILD_VERSION}" \
	org.label-schema.uri="${BUILD_PROJECT_LINK}" \
	org.label-schema.pipe-uri="${BUILD_CI_LINK}" \
	org.label-schema.vcs-ref="${GIT_COMMIT}" \
	org.label-schema.schema-version="1.0"
